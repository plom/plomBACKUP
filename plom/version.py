# Copyright (C) 2018-2020 Andrew Rechnitzer"
# SPDX-License-Identifier: AGPL-3.0-or-later

__version__ = "0.4.3.dev"
